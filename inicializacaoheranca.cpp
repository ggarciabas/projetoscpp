/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Inicialização + herança
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>

using namespace std;

class A {
private:
    int cont;
public:
    A() : cont (1){
        cout << "a() :: " << cont << endl;
    }
    int getCont () {
        return cont;
    }
};

class B {
private:
    A *a;
public:
    B() : a() { // chamada errada
        cout << "b() :: " << a->getCont() << endl;
    }
};

int main () {
    B * b = new B(); // Segmentation fault
    
    // Quando é utilizado ponteiros, a inicialização default no construtor de uma
    // classe utilizadora não retornará um ponteiro para tal, esta tem a intenção de
    // chamar o método para construção do objeto e não retornar um enredeço para
    // ele.
    
    delete b;
    return 0;
}