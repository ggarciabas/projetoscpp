/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  PThread exemplos        
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>
#include <pthread.h>
#include <stdlib.h>

using namespace std;

void* function_1 (void*);

int main ()
{
    pthread_t thread1, thread2; // criando threads
    const char* m1 = "Primeira";
    const char* m2 = "Segunda";
    
    // criando as threads
    /*
       int pthread_create(pthread_t * thread, 
                       const pthread_attr_t * attr,
                       void * (*start_routine)(void *), 
                       void *arg);
     */
    if (
         pthread_create(&thread1, NULL,
                function_1,
                (void*)m1
            )   
        ) {
        cerr << "Falha na criação da thread1." << endl;
        exit(EXIT_FAILURE);
    }
    
    if (
         pthread_create(&thread2, NULL,
                function_1,
                (void*)m2
            )   
        ) {
        cerr << "Falha na criação da thread2." << endl;
        exit(EXIT_FAILURE);
    }
    
    // iniciando threads
    /*
       int pthread_join(pthread_t th, void **thread_return);
     */
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    
    return 0;
}

void* function_1 (void* ptr) 
{
    cout << "Executando :: " << (char*)ptr << endl;
}
