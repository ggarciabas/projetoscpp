/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Gerenciamento de memória:
 *      Malloc novamente, vejamos se existe alguma validação para free().
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <stdio.h>
#include <stdlib.h>

int main () {
    int *x = (int *) malloc (sizeof(*x));
    *x = 1234;
    free(x);
    free(x); // note, esta duplicado!
    printf ("Valor de X::: %d\n", *x);    
    
    return 0;
}
