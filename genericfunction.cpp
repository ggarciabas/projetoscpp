/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Templates (tipos genéricos)
 * 
 *      Permite a criação de códigos reutilizáveis.
 *      Permite criar classes e funções genéricas, onde o tipo dos dados
 *  serão definidos pelos parâmetros.
 *      
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>

using namespace std;

template<class TIPO> void invertendo(TIPO &v1, TIPO &v2) {
    TIPO aux;

    aux = v1;
    v1 = v2;
    v2 = aux;
}

template<class TIPO> void invertendoPointer(TIPO *v1, TIPO *v2) {
    TIPO *aux;

    aux = v1;
    v1 = v2;
    v2 = aux;
}

int main() {
    int a = 1, b = 2;
    cout << "A (" << a << ")\tB(" << b << ")" << endl;
    invertendo(a, b);
    cout << "A (" << a << ")\tB(" << b << ")" << endl;
    invertendoPointer(&a, &b); // sem alterações --- pq??
    cout << "A (" << a << ")\tB(" << b << ")" << endl;

    double c = 1.9, d = 2.9;
    cout << "C (" << c << ")\tD(" << d << ")" << endl;
    invertendo(c, d);
    cout << "C (" << c << ")\tD(" << d << ")" << endl;
    invertendoPointer(&c, &d); // sem alterações --- pq??
    cout << "C (" << c << ")\tD(" << d << ")" << endl;

    char e = 'a', f = 'b';
    cout << "E (" << e << ")\tF(" << f << ")" << endl;
    invertendo(e, f);
    cout << "E (" << e << ")\tF(" << f << ")" << endl;
    invertendoPointer(&e, &f); // sem alterações --- pq??
    cout << "E (" << e << ")\tF(" << f << ")" << endl;


    return 0;
}

