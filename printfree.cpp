/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Gerenciamento de memória:
 *      Um pouco sobre malloc. O que pode ou não acontecer caso seja alocado
 *          memória, inserido um valor, liberado memoria e enfim impresso o valor
 *          da variável alocada?
 * 
 *      Bem, o valor pode ser 0 (nulo) ou também o valor inserido primeiramente.
 *      
 *      Melhor ter isso em seu controle!
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <stdio.h>
#include <stdlib.h>

int main () {
    int *x = (int *) malloc (sizeof(*x));
    *x = 1234;
    free(x);
    printf ("Valor de X::: %d\n", *x);    
    
    return 0;
}
