/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  PThread erro em C++. 
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>
#include <stdlib.h>
#include <pthread.h>

using namespace std;

void function(void*);

int main() {
    pthread_t thread;
    const char* m = "Thread";

    if (
            pthread_create(
                &thread,
                NULL,
                (void*) (&function),
                (void*) m
            )
         ) {
        cerr << "Falha na criação da thread1." << endl;
        exit(EXIT_FAILURE);
    }
    
    pthread_join(thread, NULL);

    return 0;
}

void function(void* ptr) {
    cout << "Executando :: " << (char*) ptr << endl;
}