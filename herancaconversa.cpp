/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Herança:
 *      (joke) 
 *          Desculpe, mas vc não tem e o problema eu resolvo. =P
 * 
 *      (back)
 *          Quando se fala em herança multipla, ja se fala no problema de caminhos.
 *          Vejamos:
 * 
 *                              A
 *                          ---------
 *                          |  get  |
 *                 |----|>  | *get2 | <|----|
 *                 |        ---------       |
 *              B  |                        | C
 *          ---------                      ---------
 *          |  get  |                      |  get  |
 *          | 'get2 |                      | 'get2 |
 *          ---------                      ---------
 *             /\                             /\
 *             --                             --
 *              |                              |
 *              |               D              |
 *              |           ---------          |
 *              ---------   |  get  |   --------
 *                          |  get2 |
 *                          ---------
 *  Onde:
 *      * - virtual puro
 *      ' - apenas virtual
 * 
 *  Como diferenciar em D qual método get2 executar?
 *  Por padrão ele irá escolher um caminho e executar, porém e se for necessário
 *      executar o método da classe B ao invés da C?
 * 
 *  -- Pois bem, nesse caso devemos utilizar operador de escopo '::'. Isso sim,
 *      este carinha é muito importante, pois ele permitirá escolher qual
 *      método das super-classes executar.
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>

class A {
public:

    A() {
    }

    ~A() {
    }

    virtual void get() {
        std::cout << "\tget:: a" << std::endl;
    }
    virtual void get2() = 0;
};

class B : public A {
public:

    B() {
    }

    ~B() {
    }

    void get() {
        std::cout << "\tget:: b" << std::endl;
        std::cout << "\tget:: -- chamando A" << std::endl;
        A::get();
    }

    virtual void get2() {
        std::cout << "\tget2:: b" << std::endl;
    }
};

class C : public A {
public:

    C() {
    }

    ~C() {
    }

    void get() {
        std::cout << "\tget:: c" << std::endl;
        std::cout << "\tget:: -- chamando A" << std::endl;
        A::get();
    }

    virtual void get2() {
        std::cout << "\tget2:: c" << std::endl;
    }
};

class D : public B, public C {
public:

    D() {
    }

    // não pode ser esquicido também do método destrutor como sendo virtual,
    //  estamos tratando de um estrutura polimórfica, caso o destrutor não
    // seja virtual, comportamentos indefinidos podem vir a acontecer.
    virtual ~D() {
    }

    void get() {
        std::cout << "get:: d" << std::endl;
        std::cout << "get:: -- chamando B" << std::endl;
        B::get();
        std::cout << "get:: -- chamando C" << std::endl;
        C::get();
    }

    void get2() {
        std::cout << "get2:: d" << std::endl;
        std::cout << "get2:: -- chamando B" << std::endl;
        B::get2();
        std::cout << "get2:: -- chamando C" << std::endl;
        C::get2();
    }
};

int main() {

    D *d = new D();

    std::cout << "Executando get:: (...)" << std::endl;
    d->get();
    std::cout << "Executando get2:: (...)" << std::endl;
    d->get2();

    delete d; // liberando recurso!
    return 0;
}