/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *  Gerenciamento de memória:
 *      Erros em blocos de memoria por conta de ponteiros mal gerenciados.
 *
 * Author: Giovanna Garcia Basilio (ggarciabas@gmail.com)
 */

#include <iostream>

using namespace std;

class A {
private:
    int _1;
public:
    int getValue () { return _1; } // inline function
    void setValue (int v) { _1 = v; }
};

int main () {
    A *_a = new A(); // operador para criar objetos dinamicamente
    _a->setValue(1); // -> operador para acessar metodos via ponteiro de memoria
    cout << _a->getValue() << endl;
    
    // delete _a; // (solve) liberando memoria, previnindo erros de blocos por conta da memoria alocada e não liberada.
    return 0;
}